// Team 1 ~ Comp 220 ~ TSP.java
// Our Travelling Salesman Problem

import java.util.Arrays;

public class TSP {

	final City[] cities;
	final int MAX_GEN = 500;
	static double p_mutate = 0.1;
	final int POPL_SIZE = 1000;
	final double EPSILON = 0.001;
	
	Map map;
	Tour[] new_pool, old_pool;

	TSP(int numCitiesOnSide) {
		this.cities = GenerateCities.square(numCitiesOnSide);
		Tour.tsp = this;
		this.map = new Map(this);

		old_pool = new Tour[POPL_SIZE];
		new_pool = new Tour[POPL_SIZE];
	}

	int numCities() {
		return cities.length;
	}

	void run() {
		Tour lastBestTour;	// create a holder for the last pool's best tour
		boolean epsilonTest = false;	// create a boolean for testing compareTo
		int k = 1;	//create an increment int, since for loop no longer used
		
		// create an initial random population of tours
		for (int i = 0; i < POPL_SIZE; i++) {
			old_pool[i] = Tour.rndTour();
		}

		Tour[] tmp_pool;

		// show best initial random tour
		Arrays.sort(old_pool);
		old_pool[0].display(map);
		Util.pause(200);

		// James's "exit when not improved" while loop (Update #1)
		while (!epsilonTest) {
			lastBestTour = new Tour(old_pool[0]); //set to old_pool's best tour 
			if (k % 1 == 0) { // change the 1 to what ever interval you want
				showPool(old_pool, 
						String.format("Mutate factor: %,.2f", p_mutate), 
						String.format("Gen: %3d", k), 5);
				map.gen = k;
				// display the lowest distance (i.e. fittest) tour
				old_pool[0].display(map);
				Util.pause(200);
			}//if k

			// Mitchel's changing mutation factor (Update #3)
			if (k % 20 == 0){
				p_mutate = p_mutate*1.5;
			}
			
			// reset the index; -- see below
			new_i = 0;

			for (int i = 0; i < POPL_SIZE / 2; i++) {
				do_ga(); // new children are added to new_pool
			}
			
			// Cord's elitist code (Update #2)
			for (int i = 0; i<.01*POPL_SIZE;i++) {
				new_pool[i]=old_pool[i];
			
			}
			
			// interchange the new and old pools
			tmp_pool = old_pool;
			old_pool = new_pool;
			new_pool = tmp_pool;
			// sort the old_pool in ascending order
			Arrays.sort(old_pool);
			
			// Exit if the best tour has not improved
			if (EPSILON >= (lastBestTour.distance - old_pool[0].distance)) {
				epsilonTest = true;	//sets sentinal value to true
			}//if EPSILON
			
			k++; //increment k
		}//for-k
	}//run

	int new_i = 0; // index into the new_pool

	void do_ga() {

		// * SELECT two individuals to mate
		Tour[] pair = selectTwo();

		// * CROSS-OVER
		// determine two cross-over points
		int x1, x2;
		x1 = Util.rndInt(1, numCities() - 2);
		x2 = Util.rndInt(x1, numCities() - 1);

		// determine the children by first crossing pair[0] with pair[1]
		// and then crossing pair[1] with pair[0]
		Tour t0, t1;
		t0 = pair[0].crossOver(pair[1], x1, x2);
		t1 = pair[1].crossOver(pair[0], x1, x2);

		// * MUTATE both c0 and c1
		t0.mutate();
		t1.mutate();

		// finally add c0 and c1 to the new pool
		new_pool[new_i++] = t0;
		new_pool[new_i++] = t1;
	}//do GA

	// randomly select two parents from the more fit population to mate
	Tour[] selectTwo() {
		Tour[] pair = new Tour[2];

		int k = POPL_SIZE / 2;

		pair[0] = old_pool[Util.rndInt(0, k)];
		pair[1] = old_pool[Util.rndInt(0, k)];

		return pair;
	}

	
	void showPool(Tour[] pool, String msg1, String msg2, int n) {
		System.out.println("\n"+msg1);
		System.out.println(msg2);
		for (int i = 0; i < n; i++) {
			System.out.printf("  %3d %s\n", i, pool[i]);
		}
	}

}
