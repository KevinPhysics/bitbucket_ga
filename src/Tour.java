// Team 1 ~ Comp 220 ~ Tour.java
// Represents a tour of cities

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.lang.Comparable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Tour implements Comparable<Tour> {

	static TSP tsp;

	int[] index; // indices into the array of cities in TSP.java

	double distance;

	private Tour() {
		this.index = new int[tsp.cities.length];
	}

	int length() {
		return index.length;
	}

	/**
	 * Compares the total distance of two tours and returns a negative
	 * number if the first is smaller, 0 if they are equal and a
	 * positive number if the first is bigger
	 *
	 * @param otherTour other tour to compare this to
	 * @return negative if smaller, 0 if equal, positive if bigger
	 */
	public int compareTo(Tour otherTour) {
		return (int) (this.distance) - (int) (otherTour.distance);
	}

	// the below constructor is used to make a clone
	Tour(Tour tr) {
		this();
		for (int i = 0; i < tr.length(); i++) {
			this.index[i] = tr.index[i];
		}
		this.distance = calcTourDistance();
	}

	static Tour sequence() {
		Tour tr = new Tour();
		for (int i = 0; i < tr.length(); i++) {
			tr.index[i] = i;
		}
		tr.distance = tr.calcTourDistance();
		return tr;
	}

	static Tour rndTour() {
		Tour tr = Tour.sequence();
		tr.randomize();
		tr.distance = tr.calcTourDistance();
		return tr;
	}

	void randomize() {
		randomize(1, this.length());
	}

	void randomize(int i, int j) {
		for (int k = 0; k < (j - i + 1); k++) {
			rndSwapTwo(i, j);
		}
		this.distance = calcTourDistance();
	}

	void rndSwapTwo() {
		// index 0 has the start city and doesn't change
		// hence the random numbers are from 1 onwards
		rndSwapTwo(1, this.length());
	}

	// randomly swap two cities between [i,j)
	void rndSwapTwo(int i, int j) {
		int a, b;
		int tmp;

		a = Util.rndInt(i, j-1);
		b = Util.rndInt(i, j-1);
		tmp = index[a];
		index[a] = index[b];
		index[b] = tmp;
	}

	City city(int i) {
		return tsp.cities[this.index[i]];
	}

	/**
	 * Calculates the total distance covered by a tour
	 * 
	 * @return total distance
	 */
	double calcTourDistance() {
		double distance = 0;	//Tour's distance
		
		// Calculates distance between all, but the end cities
		for (int ind = 0; ind < index.length - 1; ind++) {
			distance += city(ind).findCityDistance(city(ind + 1));
		}
		
		// Adds the distance between the end cities
		distance += city(index.length - 1).findCityDistance(city(0));
		
		return distance;
	}

	public String toString() {
		String s = String.format("%6.1f :", this.distance);
		for (int i = 0; i < this.length(); i++) {
			s += String.format("%3d", index[i]);
		}
		return s;
	}

	void mutate() {
		if (Math.random() < TSP.p_mutate) {
			// randomly flip two cities
			rndSwapTwo();
			this.distance = calcTourDistance();
		}
	}

	Tour crossOver(Tour p2, int x1, int x2) {
		
		// Child tour
		Tour c1 = new Tour();
		// Crossover segment of parent 2
		Set<Integer> mamaCrossover = new HashSet<Integer>(); 
		// Unique values in crossover segment of parent 1 (this)
		Set<Integer> uniquePapa = new HashSet<Integer>(); 
		// Ends of the child tour
		Set<Integer> childEnds = new HashSet<Integer>();


		// copy the 1st segment of p1 (this) to the child
		for(int i = 0;i < x1;i++) {
			c1.index[i]= this.index[i];
			childEnds.add(this.index[i]);
		}

		// copy the cross-over portion of p2 to the child
		for(int i = x1;i < x2+1;i++) {
			c1.index[i]=p2.index[i];
		}


		// copy the last segment of p1 (this) to the child
		for(int i=x2+1;i <this.length();i++) {
			c1.index[i]=this.index[i];
			childEnds.add(this.index[i]);
		}

		// Now we need to correct the child for any duplicate cities

		// First find out the unique elements of the cross-over segment
		// i.e., those elements of the cross-over segment of
		// p1 that are not in p2
		//COMP220 Requires use to use a hashSet to solve this problem


		for(int i = x1;i < x2+1;i++) {
			mamaCrossover.add(p2.index[i]);
		}
		
		for(int i = x1;i < x2+1;i++) {
			uniquePapa.add(this.index[i]);
		}
		
		uniquePapa.removeAll(mamaCrossover);

		// scan the two portions of p1 that have been crossed into the
		// the child for any duplicates in the crossed-over 
		// segment and if so replace with an element from the uniq list
		//Again, you are to use Java Sets here
		Iterator<Integer> myIter = uniquePapa.iterator();

		childEnds.retainAll(mamaCrossover);

		//Dana's Crossover Repair Improvement (Update #4)
		int r = Util.rndInt(0,2); //random number, either 0 or 1
		
		
		if(r == 0) { //repairs child from the beginning
			for(int i = 0; i < x1; i++) {
				if(childEnds.contains(c1.index[i])) {
					c1.index[i] = myIter.next();
				}
			}
			
			for(int i = x2+1; i < c1.index.length; i++) {
				if(childEnds.contains(c1.index[i])) {
					c1.index[i] = myIter.next();
				}
			}
		} else { //repairs child from the end
			for(int i = c1.length()-1; i > x2; i--) {
				if(childEnds.contains(c1.index[i])) {
					c1.index[i] = myIter.next();
				}
			}
			
			for(int i = x1-1; i >= 0; i--) {
				if(childEnds.contains(c1.index[i])) {
					c1.index[i] = myIter.next();
				}
			}
		}

		//calculate fitness of this child
		c1.distance = c1.calcTourDistance();
		
		return c1;
	}

	void display(Map m) {
		m.tour = this;
		m.update(m.getGraphics());
	}

	void display(Graphics g) {
		final int SIZE = 2 * Map.CITY_SIZE;
		// plot the tour
		int len = this.length();
		int x1, y1, x2, y2;
		int i;
		for (i = 0; i < len - 1; i++) {
			x1 = city(i).x;
			y1 = city(i).y;
			x2 = city(i + 1).x;
			y2 = city(i + 1).y;
			g.setColor(Color.CYAN);
			g.drawLine(x1, y1, x2, y2);
			g.setColor(Color.RED);
			g.setFont(new Font("SansSerif", Font.BOLD, 16));
			g.drawString(Integer.toString(i), x1 - SIZE, y1 + SIZE);
		}
		// close the loop
		g.setColor(Color.CYAN);
		x1 = city(len - 1).x;
		y1 = city(len - 1).y;
		x2 = city(0).x;
		y2 = city(0).y;
		g.drawLine(x1, y1, x2, y2);
		g.setColor(Color.RED);
		g.drawString(Integer.toString(i), x1 - SIZE, y1 + SIZE);

	}
}
