// Team 1 ~ Comp 220 ~ Map.java
// Creates the graphics for TSP

import javax.swing.JFrame;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Color;

@SuppressWarnings("serial")
public class Map extends JFrame {

	// The TSP object that Map will display tours for
	TSP tsp;

	// Current tour being displayed
	Tour tour = null;

	// Curent generation
	int gen = 0;
	
	// The width of our square of cities
	static final int WIDTH = 600;

	/**
	 * Constructs a map for a given TSP
	 * 
	 * @param tsp The TSP class this Map will display tours for
	 */
	Map(TSP tsp) {
		this.setSize(700, 800);
		this.setTitle("Map");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.tsp = tsp;
	}

	/**
	 * Paints the display.
	 * 
	 * Overwrites the java.awt.Window.paint method.
	 * 
	 * @param g Graphics object
	 */
	public void paint(Graphics g) {
		
		// Overwrites old image with the black background
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		
		// Displays a tour if one currently exists
		if (tour != null) {
			tour.display(g);
		}
		
		// Displays each city
		for (int i = 0; i < tsp.numCities(); i++) {
			displayCity(g, tsp.cities[i], i);
		}
		
		// Creates and sets the title for the window
		String title = String.format("Map [%3d] %s", gen,
				(this.tour == null ? "<null>" : tour.toString()));
		this.setTitle(title);
	}
	
	// Size of the circle plotted on the map
	static final int CITY_SIZE = 10;

	// Color of each city
	final Color CITY_COLOR = Color.GREEN;
	
	/**
	 * Displays each individual city
	 * 
	 * @param g Graphics object
	 * @param c City to be printed
	 * @param n City number in order around the square
	 */
	void displayCity(Graphics g, City c, int n) {
		g.setColor(CITY_COLOR);
		g.fillOval(c.x - CITY_SIZE / 2, c.y - CITY_SIZE / 2, CITY_SIZE, CITY_SIZE);
		g.setColor(Color.YELLOW);
		g.setFont(new Font("SansSerif", Font.BOLD, 16));
		g.drawString(Integer.toString(n), c.x + CITY_SIZE / 2, c.y - CITY_SIZE / 2);
		
	}
}