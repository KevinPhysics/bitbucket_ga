// Team 1 ~ Comp 220 ~ City.java
// A single city for the tours to travel to and from

class City {

	int x, y;

	City(int x, int y) {
		this.x = x;
		this.y = y;
	}

	double findCityDistance(City c) {
		double X = Math.pow((c.x - this.x),2);
		double Y = Math.pow((c.y-this.y),2);
		double distance=Math.sqrt(X+Y);
		return distance;
	}

	public String toString() {
		return String.format("<City: %3d, %3d>", this.x, this.y);
	}
	
}