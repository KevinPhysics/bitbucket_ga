// Team 1 ~ Comp 220 ~ MainDriver.java
// Driver for our TSP

// Team 1 consists of Cord Bocheff, Kevin Connors, James McMichael,
// Mitchel Miller and Dana Reigle

// Cord did the Tour's Crossover method and Update #2
// Kevin did the Tour's compareTo and distance methods and Update #5
// James did the Util methods and Update #1
// Mitchel did the City's distance method and Update #3
// Dana did the MainDriver method and Update #4

public class MainDriver {

	public static void main(String[] args) {
		int numCitiesOnSide = 0;	//will store command line argument 
		boolean intFound = false;	//is the command line argument an int?
		TSP tsp;					//Traveling Salesman Problem
		
		System.out.println("Team 1 ~ Comp 220 ~ TSP for GA with GIT");
		System.out.println("Solves the TSP for GA (made via GIT)");
		
		if(args.length > 0) { //checks if there is an argument
			intFound = true;
			
			//In case the first argument is not an int
			try {
				numCitiesOnSide = Integer.parseInt(args[0]);
			}
			catch (NumberFormatException e) {
				System.out.println("Integer for number of cities not found.");
				intFound = false;
			}
		}
		else //no command line argument
			System.out.println("Integer for number of cities not found.");
		
		if(intFound) { //runs if an integer has been found
			//makes sure the number of cities is in the interval [4,25]
			if(numCitiesOnSide < 4 || numCitiesOnSide > 25) {
				System.out.println("Integer for number of cities not in "
						+ "range [4,25].");
			}
			else { //finally runs the tsp
				tsp = new TSP(numCitiesOnSide);
				tsp.run();
			}
		}
		
		System.out.println("\n\n<<<<< Normal Termination >>>>>\n");

	}

}
