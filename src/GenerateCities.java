// Team 1 ~ Comp 220 ~ GenerateCities.java
// Generates the city array for TSP

public class GenerateCities {

	/**
	 * Generates the square of cities along with one city in the
	 * center and returns an array of City objects that have the
	 * proper coordinates
	 * 
	 * Kevin's city in the center of the square (Update #5)
	 * 
	 * @param N Number of intervals between cities on each side
	 * @return Array of cities created
	 */
	static City[] square(int N) {
			// N is the number of intervals per side of the square
		
		// Holds the cities for each side and the one in the center
		City[] cities = new City[4 * N + 1];
		
		// First city is in the Top-left corner
		int x0 = 50;
		int y0 = 130;
		
		// Gets the width of the square from the Map class
		int width = Map.WIDTH;
		
		// Gets the length of the intervals for the sides of the square
		int[] horz = new SplitInterval(x0, x0 + width, N).getIntervals();
		int[] vert = new SplitInterval(y0, y0 + width, N).getIntervals();
		
		//Index of the current city being created for the city array
		int cityNum = 0;
		
		// Top side of the square
		for (int j = 0; j < horz.length; j++) {
			cities[cityNum] = new City(horz[j], y0);
			cityNum++;
			}
		
		// Right side of the square
		for (int j = 1; j < vert.length; j++) {
			cities[cityNum] = new City(x0 + width, vert[j]);
			cityNum++;
		}
		// Bottom side of the square
		for (int j = horz.length - 1 - 1; j >= 0; j--) {
			cities[cityNum] = new City(horz[j], y0 + width);
			cityNum++;
		}
		
		// Left side of the square
		for (int j = vert.length - 1 - 1; j > 0; j--) {
			cities[cityNum] = new City(x0, vert[j]);
			cityNum++;
		}
		
		// Adds a city to the center of the square
		cities[cityNum] = new City(x0 + (width) / 2, y0 + (width) / 2);
		
		return cities;
	}

}