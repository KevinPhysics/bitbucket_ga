// Team 1 ~ Comp 220 ~ Util.java
// Provides various utilities for the rest of the program

import java.util.Random;

public class Util {
	static Random myRand = new Random();
	
	/**
	 * Calculates a random number, with lo and hi setting the range
	 * @param lo - the minimum of the range
	 * @param hi - the maximum of the range
	 * @return random number between parameters
	 */
	static int rndInt(int lo, int hi) {
		Random myRand = new Random();
		int returnNum = myRand.nextInt(hi - lo) + lo;
		return returnNum;
	}

	/**
	 * Counts the number of occurence of key in a, from start to end
	 * @param a - integer array from which key will be tested for
	 * @param key - integer which will be tested for
	 * @param start - where in the array testing will start
	 * @param end - where in the array testing will end
	 * @return number of key's occurence
	 */
	static int occurenceCount(int[] a, int key, int start, int end) {
		int cnt = 0;	//return value
		int i;			//loop control
		
		for (i= start; i<=end; i++) {
			if (a[i] == key)
				cnt++;
		}
		return cnt;
	}
	
	/**
	 * pauses the program for requested amount of time
	 * @param n - requested amount of time
	 */
	static void pause(int n) {
		try {
			Thread.sleep(n);
		} catch (Exception e) {

		}
	}

}
